## 基本概念
1. 节点: 元素，属性和文本,注释节点等12种统称节点。 元素是属性和文本的父节点
2. 元素: div  a  p  span  [方法](#yuansu)
3. 属性: href id class  [方法](#shuxing)
4. 文本: 具体的内容  [方法](#wenben)

## Node类型
1. node节点的关系
> 父子关系：parentNode  firstNode lastChild  [childNodes](#childNodes)  
> 兄弟关系：nextSibling perviousSibling  
2. 操作node节点
> appendChild()  
> replaceChild()  
> insertBefore()  
 


## Docunemt对象(doument对象表示整个html页面)

### document返回值：
|名称|区别|举例| why |
|:-:|:-:|:-:|  
|节点||document.body  //`<body>...</body>`| 当整个html中的标签元素唯一时，返回节点 |
|HtmlCollection|||
|NodeList|||

### 属性和方法
````documentElement
document == windows.document;        //document是一个全局变量，表示整个html页面
console.log(document);               //整个html页面的所有元素（doctype的声明和html树）
var doctype = document.doctype;      //doctype的声明  <!DOCTYPE html>
var html = document.documentElement; //html树  <html>...</html>
document.head  // <head>...</head>
document.body  // <body>...</body>
document.body.childNodes          // NodeLists body下的节点。
document.title // title的文本
document.links  // 所有的a标签     HTMLCollection
document.images // 所有的img标签   HTMLCollection
document.forms  // 所有的form标签  HTMLCollection
document.anchors// 所有的name特性标签  HTMLCollection

document.URL;  //全部地址
document.domain;  //域名
document.referrer;  //上一个url

document.write();
document.writeIn();
````

>  <em><strong>有了 document 类，我们就能获取html的节点元素。如 document.body.childNodes ，但是需要明白NodeList的数组结构,(如换行也是NodeList的value值 => text文本)。
为了更加便捷的获取body中的元素，我们有了Element元素对象</strong></em>


## Element对象
###  <h3 id="yuansu">元素的增删改查</h3>
|方法名| 返回值 |作用 | 举例 | 补充 |
|:-:|:-:|:-:|:-:|:-:|
| getElementsByTagName   | object(数组) | 通过元素获取元素名称 | - | - |
| getElementById         | object(数组) | 通过节点获取元素名称 | - | - |
| getElementsByClassName | object(数组) | 通过节点获取元素名称 | - | - |
| getElementsByName      | object(数组) | 通过节点获取元素名称 | - | - |
| querySelector          | object(节点文本) | 通过css选择器获取元素名称 | 示例 ||
| querySelectorAll       | object(数组) ||||
| forms                  |||||

### 属性和方法
````
var div = document.getElementById("mydiv");

div.tagName;                                                 // "DIV" 获取标签名
div.title;  <=> div.getAttribute("title")                    // 获取标签属性 标题
div.id;  <=> div.getAttribute("id")                          // 获取标签属性 id 
div.className;   <=> div.getAttribute("class")               // 获取标签属性 类
div.title = "我的盒子"  <=>  div.setAttribute("id","new_id")  //修改属性
div.removeAttribute();                                       //移除属性
````



## <h3 id="shuxing">属性的增删改查</h3>


## <h3 id="wenben">文本的增删改查</h3>


