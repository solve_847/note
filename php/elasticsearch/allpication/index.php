<?php
require_once("./BaseController.php");

class EsController{
    public $esClient = null; 

    public function __construct(){
        $this->esClient = BaseController::getInstance();
    }
    //创建信息，写入es 
    public function create(){

        $params = [
            'index' => 'forum_detail',
            'id'    => 3,
            'body'  => ['testField' => [
                    "id" => 3,
                    "title" => "文章标题3",
                    "content" => "文章内容的详情3",
                    "create_time" => date("Y-m-d H:i:s",time()),
                ]
            ]
        ];
        
        $response = $this->esClient->index($params);
        print_r($response);
    }
    //查询
    public function index(){
        $params = [
            'index' => 'my_index',
            'id'    => 'my_id'
        ];
        
        $response = $this->esClient->get($params);
        //获取_source 中的资源
        //$source = $this->esClient->getSource($params);
        //print_r($source);
        print_r($response);
    }

    //文档搜索
    public function documentSearch(){
        $params = [
            'index' => 'forum_detail',
            'body'  => [
                'query' => [
                    'match' => [
                        'testField.title' => '章',
                    ]
                ]
            ]
        ];
        
        $response = $this->esClient->search($params);
        print_r($response);
    }
    /**
     *  文档搜索 bool，模糊搜索
     */
    public function documentSearchBool(){
        $params = [
            'index' => 'forum_detail',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [ 'match' => [ 'testField.title' => '文章' ] ],
                            [ 'match' => [ 'testField.content' => '文章' ] ],
                        ]
                    ]
                ]
            ]
        ];
        $response = $this->esClient->search($params);
        print_r($response);
    } 

    /**
     * @note 文档删除
     */
    public function delete(){
        $params = [
            'index' => 'my_index',
            'id'    => 'my_id'
        ];
        
        $response = $this->esClient->delete($params);
        print_r($response);
    }

}

$esCon = new EsController();
if($argv[1]){
    call_user_func(array($esCon, $argv[1]));
    die;
}else{
    echo "请输入方法";
}

