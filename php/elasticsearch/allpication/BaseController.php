<?php 
require '../vendor/autoload.php';
use Elasticsearch\ClientBuilder;

class BaseController{

    private static $instance;
    private function __construct(){}
    private function __clone(){}
    public static function getInstance(){
        if(!(self::$instance instanceof self)){
            self::$instance = ClientBuilder::create()->build();;
        }
        return self::$instance;
    }

}