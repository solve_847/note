<?php
/**
 * @note 统计文章的字数，图片，视频
 * @param string $content 富文本内容
 * @return array
 */
class Article{
    private function getContentlenght($content){
        $pattern_img = "/img([^>]*)src\s*=\s*/i";
        preg_match_all($pattern_img,$content,$match_img);
    
        $pattern_video = "/video([^>]*)src\s*=\s*/i";
        preg_match_all($pattern_video,$content,$match_video);
    
        $pattern_delhtml = "/<[^>]+>/i";
        preg_match_all($pattern_delhtml,$content,$match_delhtml);
        if(is_array($match_delhtml)){
            $match_delhtml[0] = array_unique($match_delhtml[0]);
            foreach($match_delhtml[0] as $v){
                $content = str_replace($v,'',$content);
            }
        }
        $content = str_replace('&nbsp;','',$content);  //去除html标签后的内容
    
        //汉字
        $pattern_cn = "/[\x{4e00}-\x{9fa5}]+/u";
        preg_match_all($pattern_cn,$content,$match_cn);
        $cnnum = mb_strlen(str_replace(' ','',implode(' ',$match_cn[0])));
    
        //英文字母
        $pattern_en = "#[a-zA-Z]+#iu";
        preg_match_all($pattern_en,$content,$match_en);
        $ennum = strlen(str_replace(',','',implode(',',$match_en[0])));
    
        //总字符
        $allnum = mb_strlen($content);
    
        $result = [
            'img_num' => empty(count($match_img[0])) ? 0 : count($match_img[0]),
            'video_num' => empty(count($match_video[0])) ? 0 : count($match_video[0]),
            'cn_num' => empty($cnnum) ? 0 : $cnnum,
            'en_num' => empty($ennum) ? 0 : $ennum,
            'all_num' => empty($allnum) ? 0 : $allnum
        ];
        return $result;
    }
}
