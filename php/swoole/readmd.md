## csp编程模型
1. 链接
> https://blog.csdn.net/gaobinzhan/article/details/105807457

2. 协程

3. 协程间通信

## swoole异步和协程的区别
### 协程：
1. 不需要操作系统参与，创建销毁和切换的成本非常低，遇到io会自动让出cpu执行权，交给其它协程去执行。
2.  ![协程示意图](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9xaW5pdS5nYW9iaW56aGFuLmNvbS8yMDIwLzA0LzI3LzFkZGQ2ODIxY2UwYTkucG5n?x-oss-process=image/format,png)

## 异步：
1. 在Server程序中如果需要执行很耗时的操作，比如一个聊天服务器发送广播，Web服务器中发送邮件。如果直接去执行这些函数就会阻塞当前进程，导致服务器响应变慢。
Swoole提供了异步任务处理的功能，可以投递一个异步任务到TaskWorker进程池中执行，不影响当前请求的处理速度

2. 