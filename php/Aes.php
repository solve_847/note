<?php 
class Aes
{
    //加密KEY(请设置为32位)
    private $_secret_key = '';
    public function __construct()
    {
        $this->_secret_key = config('ase_secret_key');
    }

    /**
     * @加密方法
     * @param $data
     * @return string
     */
    public function encrypt($data)
    {
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_256, '', MCRYPT_MODE_CBC, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $this->_secret_key, $iv);
        $encrypted = mcrypt_generic($td, $data);
        mcrypt_generic_deinit($td);
        return $iv.$encrypted;
    }

    /**
     * 解密方法
     * @param $data
     * @return string
     */
    public function decrypt($data){
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_256,'',MCRYPT_MODE_CBC,'');
        $iv = mb_substr($data,0,32,'latin1');
        mcrypt_generic_init($td,$this->_secret_key,$iv);
        $data = mb_substr($data,32,mb_strlen($data,'latin1'),'latin1');
        $data = mdecrypt_generic($td,$data);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return trim($data);
    }

    //简易加解密
    /**
     * [encrypt 加密]
     */
    public function encrypt($data,$iv='5c4264e37e0fd183', $salt)
    {
        return base64_encode(openssl_encrypt($data,'AES-256-CBC',$salt,OPENSSL_RAW_DATA, $iv));
    }
 
    /**
     * [decrypt 解密]
     */
    public function decrypt($data,$iv='5c4264e37e0fd183', $salt)
    {
        return openssl_decrypt(base64_decode($data),'AES-256-CBC',$salt,OPENSSL_RAW_DATA, $iv);
    }
}
