<?php
    //  composer require php-amqplib/php-amqplib --ignore-platform-reqs
/**
 * 第一个工作模式来喽：
 *  Hello World!!!
 */
require_once __DIR__ . '/../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

    $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $channel->queue_declare('hello', false, false, false, false);

    $time = date('Y-m-d H:i:s',time());
    $msg = new AMQPMessage('Hello World!'.$time);
    $channel->basic_publish($msg, '', 'hello');

    //$channel->basic_publish($msg, '', 'hello');

    echo " [x] Sent 'Hello World!'\n";

    $channel->close();
    $connection->close();
?>