<?php
/**
 * rabbitmq 基础类库
 * credit by felix
 */


class RabbitMq{

  static private $_instance = null ;//私有的静态实例
  static private $_conn;//私有的连接
  static private $routing_key;//routing key
  static private $q ;//队列实例
  static private $ex ;//交换机实例
  static private $queue;//队列名



  /**
   * 公开的方法获取实例
   * @return [type] [description]
   */
  public static function getInstance($config){
    $config = include(__DIR__.'/config/rabbitmq.php');
    if (self::$_instance == null) {
          self::$_instance = new self($config);
          return self::$_instance;
    }
    return self::$_instance;
  }
