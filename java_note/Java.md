#   Java

##  1. java基础

### 1.1 数据类型

#### 1.1.1 基本数据类型
   ```
      1.  boolean
      2.  char  
      3.  byte 、short、int、long  
      4.  float 、 double
   ```

#### 1.1.2 名词解释

- JRE：(java  run environment) java程序运行环境，包含jvm和所需的核心类库。

- JDK：(java Development kit) java程序开发工具包，包括JRE和开发人员使用的工具（javac.exe编译工具）和（java.exe开发工具）

- JVM：保证java程序的跨平台运行

  ````
  JAVA的JVM的内存可分为3个区：堆(heap)、栈(stack)和方法区(method) 
  
  堆区: 
  1.存储的全部是对象，每个对象都包含一个与之对应的class的信息。(class的目的是得到操作指令) 
  2.jvm只有一个堆区(heap)被所有线程共享，堆中不存放基本类型和对象引用，只存放对象本身 
  栈区: 
  1.每个线程包含一个栈区，栈中只保存基础数据类型的对象和自定义对象的引用(不是对象)，对象都存放在堆区中 
  2.每个栈中的数据(原始类型和对象引用)都是私有的，其他栈不能访问。 
  3.栈分为3个部分：基本类型变量区、执行环境上下文、操作指令区(存放操作指令)。 
  方法区: 
  1.又叫静态区，跟堆一样，被所有的线程共享。方法区包含所有的class和static变量。 
  2.方法区中包含的都是在整个程序中永远唯一的元素，如class，static变量。 
  ````


### 1.2 数组

1. 定义

   > 数组是一种 【存储同一数据类型】【在内存上连续排列】的 数据结构，访问很快  

2. 声明 创建 使用  

   ````java
   int[]  num;
   
   num = new int[3]; 
   
   num[数组key值];
    
   int data[] = {1, 2, 4, 545, 11, 32, 13131, 4444};
   ````

3. 匿名数组

   ````java
   new int[3]{1,2,3}
   ````

4. 多维数组  

   ````java
   int[][] num;
   ````

  ### 1.3  类与对象

- 继承：

  1. extends 继承  成员访问：直接访问

  2. this 本类引用

  3. super 父类引用
  4. 调用子类前，会先调用父类的无参构造方法

##  2. 深入String 

### 2.1 字符串特点和比较

- String 字符串创建后不可修改，但可共享【在字符串池中共享】
- 字符串效果上相当于字符数组（char[]）,底层是字节数组(  byte[] )
- ==比较的是堆内存中的地址

- equals比较的是，字符串内容是否一样

### 2.2 String的构造方法和成员方法

- 构造方法

  1.  public String() 【不论字符是否相同，JVM都会重新创建】

  2.  public String(char[] chs) 根据字符数组内容，创建字符串对象【不论字符是否相同，JVM都会重新创建】

     ````java
     char[] chs = {'a','b','c'}
     String s = new String(chs);
     ````

  3. public Sring(Byte[] bys) 根据字节数组内容，创建字符串对象 【不论字符是否相同，JVM都会重新创建】

     ````java
     byte[] bys = {97, 98, 99};
     String s = new Sring(bys);
     System.out.orintln(s); // abc
     ````

  4.  String s = "abc";  直接复制创建对象   【如果字符相同，则JVM只会**创建一次**，并在字符串池中进行维护】

#### 2.2.1 compareTo

````java
public static void main(String[] args) {
    String str1 = "abc";
    String str2 = "abA";
    System.out.println(str1.compareTo(str2));  //返回字符串ASCLL码的差值   34
}
````

#### 2.2.2 字符串拼接  +

#### 2.2.4  其他

1. length()

2. indexOf() 、 lastIndexOf()

   ````java
   str.IndexOf(String char);  // 返回这个字母的位数，从0开始,不存在返回-1
   str.IndexOf(String char,Int fromIndex);  // 返回这个字母的位数，从0开始
   ````

3.  startWith() 、 endWith()  

   > 判断字符串的开始和结束 boolean

4. charAt

   > `charAt(int index)` 返回指定索引处的 `char`值。

5. substring

   > public String substring(Int beginIndex)
   >
   > public String substring(Int beginIndex, endIndex)

6. split

   > public String[] split(String regex)   

7. replace

   > public String replace(String oldString, String newString)
   >
   > replaceFirst()
   >
   > replaceAll()

8. repeat

   > public String repeat(Int count)

9. toLowerCase()

10. toUpperCase()

13.  遍历

    ````java
    String s = "终于肯向中国低头？华盛顿传出重大消息，美国将撤销对华两大禁令";
    for (int i=0; i < s.length(); i++ ){
        System.out.print(s.charAt(i));
    }
    ````
    
    

###  2.3  buffer and builder 

- 和String类不同的是，StringBuffer 和 StringBuilder 类的对象能够被多次的修改，并且不产生新的未使用对象。
- StringBuilder类也代表可变字符串对象。实际上，StringBuilder和StringBuffer基本相似，两个类的构造器和方法也基本相同。不同的是：**<u>StringBuffer是线程安全的</u>**，而StringBuilder则没有实现线程安全功能，所以性能略高。
- ![string是不可改变的](./images/17cf6c28ad820bab7121da244c3eaf3.png)
- ![stringBuffer是可改变的](./images/4300ef8b8ec3f655e0428df15db1260.png)

####  	2.3.1 StringBuffer

1. 线程安全

2.  举例

   ````java
   /**
    *
    * 序号	方法描述
    * 1	public StringBuffer append(String s)
    * 将指定的字符串追加到此字符序列。
    * 2	public StringBuffer reverse()
    * 将此字符序列用其反转形式取代。
    * 3	public delete(int start, int end)
    * 移除此序列的子字符串中的字符。
    * 4	public insert(int offset, int i)
    * 将 int 参数的字符串表示形式插入此序列中。
    * 5	replace(int start, int end, String str)
    * 使用给定 String 中的字符替换此序列的子字符串中的字符。
    **/
   public class Test{
   
       public static void main(String args[]){
          StringBuffer sBuffer = new StringBuffer(" test");
          sBuffer.append(" String Buffer");
          System.out.println(sBuffer);   //  test String Buffer
      }
   }
   
   ````

   

#### 	2.3.2 StringBuilder


- 可更改字符串，线程不安全

- 构造方法

  ````java
  //初始化空白字符
  StringBuilder sb = new StringBuilder();
  //初始化特定字符
  StringBuilder sb1 = new StringBuilder("hello");
  ````

- 方法  
  	1. reverse
   2. append

## 3. 集合

### 3.1 集合接口和实现类

#### 3.1.1 集合框架的基本结构
1. Collection实现了List，Set，Queue三大类  
2. ![集合接口和实现类结构](https://www.runoob.com/wp-content/uploads/2014/01/2243690-9cd9c896e0d512ed.gif)



#### 3.1.2 迭代

1. Iterator

   ````java
   /**
    * Iterator<String> iterator = lst.iterator();
    * iterator.hasNext()
    * iterator.next()
    
    **/
   public static void main(String[] args) {
       List<String> lst = new ArrayList<String>();
       lst.add("aaa");
       lst.add("bbb");
       lst.add("ccc");
       lst.add("ddd");
       lst.add("eee");
       lst.add("fff");
       Iterator<String> iterator = lst.iterator();
       //iterator.hasNext()如果存在元素的话返回true
       while(iterator.hasNext()) {
           //iterator.next()返回迭代的下一个元素
           System.out.println(iterator.next());
       }
   }
   ````

2. 遍历

   ````java
   public static void main(String[] args) {
       List<Integer> num = new ArrayList<Integer>();
       num.add(1);
       num.add(9);
       num.add(5);
       for (Integer i: num){
           System.out.println(i);
       }
   
       Collection<String> str = new ArrayList<>();
       str.add("1");
       str.add("2");
       str.add("3");
       Iterator<String> it = str.iterator();
       while (it.hasNext()){
           System.out.println(it.next());
       }
   
       for (int i=0; i<str.size(); i++){
           System.out.println(((ArrayList<String>) str).get(i));
       }
   }
   ````

   

4. Collection集合的常用方法
   - add()
   - remove()
   - contains()
   - isEmpty()
   - size()
   - clear()

### 3.2 数据结构

#### 链表（Linked List）

- [链表介绍](https://zhuanlan.zhihu.com/p/29627391)

#### 栈（stack）

#### 数组（ArrayList）

定义

- 数组是一种 【存储同一数据类型】【在内存上连续排列】的 数据结构，访问很快  

优点：

- 按照索引查询元素的速度很快；
- 按照索引遍历数组也很方便。

缺点：

- 数组的大小在创建后就确定了，无法扩容；
- 数组只能存储一种类型的数据；

添加、删除元素的操作很耗时间，因为要移动其他元素。

#### 队列（Queue）

#### 树（Tree）

#### 堆 （Heap）

#### 哈希表（Hash Table）

- [哈希表知乎链接](https://zhuanlan.zhihu.com/p/95156642)

- JDK8前底层是数组和链表，JDK8后，在长度较长时，底层做了优化

- hash函数就是根据key计算出应该存储地址的位置，而**哈希表**是基于哈希函数建立的一种查找表

- 哈希表的本质是个数组，一般来说，实现哈希表可以采用两种方法：

  1.  数组+链表

  2. 数组+二叉树

- 存储过程：

  1. 先在堆内存中分配一个数组，当往哈希表中添加数据时，根据哈希函数生成数组的key，数据作为entry存储为值

  2. 当有新的数据时，且哈希函数生成的key相同，则数据根据拉链法（或开放寻址法），存储成链表结构。

  3.  ![存储示意图 ](./images/v2-2abff58382590a9d9f0578e918b44a2a_720w.jpg)

- 哈希表的扩容

  这里一般会有一个增长因子的概念，也叫作负载因子，简单点说就是已经被占的位置与总位置的一个百分比，比如一共十个位置，现在已经占了七个位置，就触发了扩容机制，因为它的增长因子是0.7，也就是达到了总位置的百分之七十就需要扩容。

  还拿HashMap来说，当它当前的容量占总容量的百分之七十五的时候就需要扩容了。

#### 图（Graph）



### 3.3 List

- ArrayList 查询快   LinkedList 增删快

- list共有方法
  1. add()
  2. remove()
  3. set()
  4. get()

#### 3.3.1 ArrayList

- 定义

  > 底层实现是采用数组来保存元素，单向链表, 特点：查询快，增删慢

- 构造方法

  ArrayList<E>  al = new ArrayList<E>();

- 方法：

1.  增

   - public boolean add(E e)
   
   - public boolean addAll(int index, Collection<E> c)

2. 删

   - public E remove(int index)

3. 改

   - set 

4. 查

   - get
   - indexOf()
   
   - lastINdexOf()
   
   - subList(int fromIndex, int endIndex)
   
5. 其他

   - size()

   - isEmpty()

   - contains()

   - toArray()


#### 3.3.2 LinkedList

- 定义

  > 底层采用双向链表来保存元素，增删快，查询慢
  
- 方法

   1. 增
      - addFirst(E)  
      - addLast(E)
   2. 查
      - getFirst(E)
      - getLast(E)
   3. 删
      - removeFirst(E)
      - removeLast(E)

### 3.4 set

- set 方法和collection相同
- 遍历 增强for遍历，iterator遍历，无索引，不能使用索引遍历
- 哈希值：JDK根据对象的地址，字符串，数字算出来的int类型的数值
  - public int hashCode();  返回对象的hash码值，同一个对象只有一个hash值  
  - "重地".hashCode()    "通话".hashCode()  一致

#### 3.4.1 HashSet

- 定义
   - 底层通过哈希表实现，允许空元素，不保证迭代顺序.
   
   - 可以通过iterator和增强器遍历
#### 3.4.2 LinkedHashSet

- 继承HashSet，由哈希表和链表实现的接口
- 存储和读取顺序一致，且不重复元素

#### 3.4.3 TreeSet


- 

### 3.5 Map

- php中的关联数组

#### 3.5.1HashMap

- 方法

  - put()
  - remove()
  - clear()
  - containsKey()
  - containsValue()
  - isEmpty()
  - size()
  - get(key)
  - Set<K> keySet()  //获取key
  - values()
  - Set<Map,Entry<k,v>> entrySet()  //获取所有键值对对象集合
- 遍历
  - 增强for循环



### 3.6 Collections

   - 针对集合操作的工具类
   #### 3.6.1 方法

   1. sort

   2. reverse

   3. shuffle

      


## 4. I/O操作
#### 4.1 创建目录

1. public boolean mkdir();
2. public  boolean mkdirs();  创建父目录和子目录
3. File.separator  路径分隔符
4. createNewFile()  创建文件
5. exists();
6. canRead()    canWrite()   isHidden()
7. public String[]  list()  得到数组
8. public File[]  listFiles()  得到文件对象
9. isDirextory()
10. delete()

````java
public static void mkdirtest(){
    File mydir = new File("./src/main/java/com/ssmtest/mkdirtest");
    try {
        if(!mydir.exists()){
            mydir.mkdir();
        }
    }catch (Exception e){
        System.out.println("已存在");
        e.printStackTrace();
    }
}
````

####  4.2  创建文件并写入

````java
public static void mkfiletest(){
    String str1 = "./mkdirtest/1.txt";
    File myFilePath = new File(str1);
    try {
        if (!myFilePath.exists()) {
            myFilePath.createNewFile();  //创建文件
        }
        FileWriter resultFile = new FileWriter(myFilePath);
        PrintWriter myFile = new PrintWriter(resultFile);  //写入
        myFile.println("锄禾日当午");
        myFile.close();
    }
    catch (Exception e) {
        System.out.println("新建文件操作出错");
        e.printStackTrace();
    }
}
````

#### 4.3 删除文件

````java
public static void deletedirtest(){
    String str1 = "mkdirtest";
    File myDelFile = new File(str1);
    try {
        myDelFile.delete();  //有子文件的不能删除
    }
    catch (Exception e) {
        System.out.println("删除文件操作出错");
        e.printStackTrace();
    }
}
````

#### 4.4  遍历目录

##### 4.4.1 遍历子目录，

````java
/**
 * 遍历子目录，子目录下的目录不遍历
 **/
public static void readtest(){
    String str1 = "mkdirtest";
    File myDelFile = new File(str1);
    File[] files = myDelFile.listFiles();
    System.out.println(Arrays.toString(files));  //[mkdirtest\1, mkdirtest\2]
}
````

#### 4.5 写入

````java
public static void writetest(){
    try {
        String str1 = "1.txt";
        FileWriter fw = new FileWriter(str1);
        fw.write("这里是输入的内容");
        fw.flush();
        fw.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
````

#### 4.6 逐行读取

````java
````

#### 4.7 流式I/O

- 字节流相关的类分别集成 抽象类  InputStream  OutStream
- 字节数组输入/输出流 ByteArrayInputStream  ByteArrayOutStream
- 文件输入/输出流 FileInputStream FileOutStream 
- 过滤流 FilterInputStream()   FilterOutStream()
- 





## 5. 日期时间

1. 获取日期和时间戳

   ````java
   public static void datetest(){
       Date date = new Date();
       System.out.println(date);  // Thu Jun 17 14:15:18 CST 2021
       SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
       System.out.println(ft.format(date)); // 星期四 2021.06.17 at 02:18:47 下午 CST
   
       SimpleDateFormat fta = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
       System.out.println(fta.format(date)); // 2021-06-17 02:21:45
       long time = System.currentTimeMillis();
       System.out.println(time);  //1623912242844  毫秒时间戳
   
       SimpleDateFormat sdf = new SimpleDateFormat("D");
       System.out.println(sdf.format(date)); //一年中的第几天
   }
   ````

   

## 6. 网络编程

### 6.1 网络编程入门

#### 1. InetAddress

### 6.2 UDP通讯程序

#### UDP释义：

[UDP说明](https://blog.csdn.net/aa1928992772/article/details/85240358)


### 6.3 TCP通讯程序







##  其他

#### 1 线程安全

> 线程安全就是多线程访问时，采用了加锁机制，当一个线程访问该类的某个数据时，进行保护，其他线程不能进行访问直到该线程读取完，其他线程才可使用。不会出现数据不一致或者数据污染。线程不安全就是不提供数据访问保护，有可能出现多个线程先后更改数据造成所得到的数据是脏数据

问：

什么情况下多线程



####  2. 位 字节 字符 

- 位：数据存储的最小单位
- 字节： 8位
- 字符： utf-8 3字节    gbk 2字节





# Maven

### 1 安装

-  Maven 3.3 要求 JDK 1.7 或以上
- 查询JDK版本： java -version
- mvn -v

### 2 本地目录和远程镜像配置

- 在\maven\conf\setting.xml中，localRepository是本地仓库配置，可以使本地的项目共同使用jar包

  >  <!--配置本地的maven目录-->>
  >
  >  <localRepository>D:\java\maven\maven\maven\repository</localRepository>

  

- 在\maven\conf\setting.xml中，localRepository是本地仓库配置，可以使本地的项目共同使用jar包

  ````xml
  <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <!--central: 中央,即jar包的来源-->
      <mirrorOf>central</mirrorOf>
  </mirror>
  ````

  ### 3.目录结构

​	![maven项目的目录结构](https://www.runoob.com/wp-content/uploads/2018/09/1536129535-6460-structure.jpg)

  

   **1. 创建Maven的普通Java项目：**

   ```
   mvn archetype:create
      -DgroupId=packageName
      -DartifactId=projectName
   ```

   **2. 创建Maven的Web项目：**

   ```
   mvn archetype:create
      -DgroupId=packageName
      -DartifactId=webappName
      -DarchetypeArtifactId=maven-archetype-webapp
   ```

### 3. 指令

**1. 编译源代码：**

```
mvn compile
```

**2. 编译测试代码：**

```
mvn test-compile
```

**3. 运行测试:**

```
mvn test
```

**4. 产生site：**

```
mvn site
```

**5. 打包：**

```
mvn package
```

**6 在本地Repository中安装jar：**

```
mvn install
例：installing D:\xxx\xx.jar to D:\xx\xxxx
```

**7. 清除产生的项目：**

```
mvn clean
```

**8. 生成idea项目：**

```
mvn idea:idea
```

**9. 只打jar包:**

```
mvn jar:jar
```

**10.查看当前项目已被解析的依赖：**

```
mvn dependency:list
```

**11.上传到私服：**

```
mvn deploy
```

**20. 强制检查更新，由于快照版本的更新策略(一天更新几次、隔段时间更新一次)存在，如果想强制更新就会用到此命令:** 

```
mvn clean install-U
```

**21. 源码打包：**

```
mvn source:jar
或
mvn source:jar-no-fork
```

- mvn compile与mvn install、mvn deploy的区别
   1. mvn compile，编译类文件
   2. mvn install，包含mvn compile，mvn package，然后上传到本地仓库
   3. mvn deploy,包含mvn install,然后，上传到私服

### 4. idea





# SpringMVC



## 1. 接收参数

### 1.1 接收json

[接收json网页链接](https://www.cnblogs.com/zzsuje/articles/10329948.html)

