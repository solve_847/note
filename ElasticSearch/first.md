# ElasticSearch
## 基础入门
### 数据的数据和输出
    - 文档元数据
        1. _index 【索引】 类似于数据表名
        2. _type  数据是松散的组合在一起的，如帖子，点赞数，评论数等。 type可以补充描述
        3. _id 自己生成或者ElasticSearch自动生成
        4. _source json资源

### 分布式存储文档
    - 分片
        1. 分片: 在ES中所有数据的文件块，也是数据的最小单元块
        2. 数据分配的分片： shard = hash(routing) % number_of_primary_shards
        ````
            routing 是一个可变值，默认是文档的 _id ，也可以设置成一个自定义的值。 routing 通过 hash 函数生成一个数字，然后这个数字再除以 number_of_primary_shards （主分片的数量）后得到 余数 。这个分布在 0 到 number_of_primary_shards-1 之间的余数，就是我们所寻求的文档所在分片的位置。
        ````