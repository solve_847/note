### 元素分类
| 分类 | 列举 | 宽 | 高 | 行高 | margin | padding | border | 占行 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|行内元素|| 无宽 | 无高 | 有行高，但优先使用父块元素的行高  | 有margin-left margin-right，没有margin-top margin-bottom | padding的上下左右都有 | 有 | 共享一行 |
|块元素  ||   宽 |  高 | 有行高  | margin的上下左右都有 | padding的上下左右都有 | 有  | 独占一行 |
|行内块元素  | img input td |   宽 |  高 | 无行高  | margin的上下左右都有 | padding的上下左右都有 | 有  | 共享一行 |


### css声明属性
1. 定位属性 position float
2. 自身属性 盒子模型
3. 文本属性
4. 颜色属性
5. C3中新增属性

### 水平垂直居中
1. 块元素的水平居中：
  > margin: 0 auto;
2. 行内元素水平居中
  > line-height: 行高;
  > 行高定义：两行字之间的距离 + 字体的高度
  #### line-height特点: 
      1. 行内元素不具备行高，当给行内元素设置行高时，行内元素所在的父盒子元素将具备行高；
      2. 同一个父元素内，当一个行内元素的行高和父元素高度一样，则所有的子元素都将垂直居中；
      ````html
        <style>
            .div_one {
                background-color: pink;
                width: 500px;
                height: 100px;
                line-height:200px;
                font-size: 18px;
            }
            .one {
                /**
                * 父块元素 和 子行内元素都有行高 ，则采用父元素的行高；
                * 父块元素无行高 ， 子行内元素有行高 ，则采用子元素的行高；
                * 当给父元素设置行高，所有的子行内元素都具有行高
                */
                line-height: 100px;
                background-color: #00FF00;
            }
        </style>
        <div>
            <p>
                <span class="one">李荣浩</span>
                <a>李宇春</a>
            </p>
        </div>
      ````
  >> 
3. 块元素的水平居中
  >

### 常用标签列表
| 标签 | 样式 | 作用 | 补充 |
|:-:|:-:|:-:|:-:|
| a         | text-decoration:none; | 下划线 | |
| a:visited |  | 点击过 | |
| a:hover   |  | 悬浮   | |
| a:active  |  | 点击 | |
| list      | list-style:none | 去除点 ||
| border    |border-collapse:collapse;|合并边框| collapse 崩溃 |
| border    | border-spacing:0 | 边框间距 | |
| overflow  | visible、hidden、scroll、auto、inherit、no-display、no-content | 溢出 | [overflow详细](#overflow) |
| font      ||||
| text-align | center |  ||
| cursor    | pointer | 鼠标手势 ||
| float     | 浮动 |  | [float详细](#float) |
| transition | 圆角 |  | [transition详细](#transition) |
| position  | 定位  |  | [position详细](#position) |
| z-index   |   |  | |
| display   |   |  | |

#### <span id="overflow">overflow详细</span>
1. 使用场景： 
 > 当一个元素固定为某个特定大小，但内容在元素中放不下，此时就可以利用overflow属性来控制这种情况。应用于块级元素、表单元格  
2. visible 块元素外也显示  hidden 块元素外隐藏 scroll 滚动条 auto(滚动条)
3. ？？？？

#### <span id="float">float详细</span>
#### <span id="transition">transition详细</span>
1. 常用
````
  transition: all 3s;
  -webkit-transition: all 1s;
  -moz-transition: all 1s;
  -ms-transition: all 1s;
````
#### <span id="position">position详细</span>

#### 清除浮动
1. 当父元素没有确定高度时，子元素浮动，则需要清除浮动，达到撑起父元素高度的目标
2. 清除浮动的方法
  > 在子元素的最后加上 br 清除浮动
  > 父块元素添加overflow  overflow:hidden  




#### float 浮动
> 1.设置的浮动样式只对元素本身有效果。对其他的兄弟没有影响。
> 2.标准的文档流可以看做是三维坐标轴（z=0）的平面。
> 3.浮动的文档流可以看做是三维坐标轴（z=1）的平面。因此 浮动会遮盖标准的文档流的元素。但是遮不住标准文档流的内容
> 4.margin:0 auto 对浮动盒子的水平居中无效果。只能用于z=0
> 5. 清除浮动，作用是使盒子独占一行



#### display
none block inline inline-block 
none（隐藏后不保留位置） block 常用于块元素的显示隐藏

visibility: hidden(隐藏后保留位置，常用于广告) visible 
overflow： hidden 对溢出的内容进行隐藏
### question
