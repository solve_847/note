#### 块元素
### 元素分类
| 分类 | 列举 | 宽 | 高 | 行高 | margin | padding | border | 占行 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|行内元素|| 无宽 | 无高 | 有行高，但优先使用父块元素的行高  | 有margin-left margin-right，没有margin-top margin-bottom | padding的上下左右都有 | 有 | 共享一行 |
|块元素  ||   宽 |  高 | 有行高  | margin的上下左右都有 | padding的上下左右都有 | 有  | 独占一行 |
|行内块元素  | img input td |   宽 |  高 | 无行高  | margin的上下左右都有 | padding的上下左右都有 | 有  | 共享一行 |

#### position
|值| 作用 | 文档流 | 使用场景 |
|:-:|:-:|:-:|:-:|
| static   | 默认 | 不脱离文档流 |  |
| relative | 相对(当对于自身，<strong>保留原位置</strong>) | <strong>不</strong>脱离文档流 |  |
| absolute | 绝对(相对于最近的<strong>有定位的</strong>父盒子) | 脱离文档流 |  |
| fixed    | 弹性 | 脱离文档流 |  |

#### z-index
| 定位层级  
| 

#### opacity 
| 透明度

#### 边偏移
> top bottom right left  
> 边偏移的正负问题  
>> top以定位相对元素的上边为x=0，以相对定位元素的可视区域为正。  与bottom互斥  
>> bottom以定位相对元素的下边为x=0，以相对定位元素的可视区域为正。 与top互斥  
>> left以定位相对元素的左边为y=0，以相对定位元素的可视区域为正。 与right互斥  
>> right以定位元相对素的右边为y=0，以相对定位元素的可视区域为正。 与left互斥  
````
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        div.one {
            position: relative;
            width: 100%;
            height: 500px;
            background-color: pink;

        }
        div.two{
            width: 100px;
            height: 100px;
            background-color: aqua;
            position: absolute;
            top:0px;
            bottom:0px;
            /* left:0px; */
            /* right:0px; */
        }
    </style>
    <div class="one">
        <div class="two"></div>
    </div>
````


1. 作用：固定盒子的位置 static relative absolute fixed
2. 边偏移： top bottom right left
3. > relative 相对于自身进行定位  ---------- 原来的位置仍然在，不脱离文档流  
   > fixed  相对于浏览器------------  不占有原来的位置，脱离文档流  
   >> 固定定位紧贴版心的右边缘  left:50% (浏览器宽度)  margin-left：版心的一半；    
   
   > absolute 相对于父元素(相对于最近的有定位的父元素定位)  ------------  不占有原来的位置，脱离文档流  
4. 子绝父相（子元素绝对定位 父元素相对定位）
   > 父元素使用相对定位，则占有文档流原来的位置，后面的元素不受影响。  
   > 子元素使用绝对定位，脱离文档流，不影响后面的兄弟盒子的定位  
5. z-index 叠放顺序 只对有定位的盒子有效
6. 使用了absolute定位的盒子，使用margin:0 auto; 不能水平居中
7. 固定定位的元素（块元素和盒内元素都可以设定宽 高）
8. 固定定位的元素会压住下面的盒子，但是压不住盒子里的内容